import {RouterModule, Routes} from "@angular/router";

import {AdminGuard, LoginGuardGuard} from "../services/service.index";

import {PagesComponent} from "./pages.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ProgressComponent} from "./progress/progress.component";
import {Graficas1Component} from "./graficas1/graficas1.component";
import {AccountSettingsComponent} from "./account-settings/account-settings.component";
import {PromesasComponent} from "./promesas/promesas.component";
import {RxjsComponent} from "./rxjs/rxjs.component";
import {ProfileComponent} from "./profile/profile.component";
import {UsuariosComponent} from "./usuarios/usuarios.component";
import {HospitalesComponent} from "./hospitales/hospitales.component";
import {MedicosComponent} from "./medicos/medicos.component";
import {MedicoComponent} from "./medicos/medico.component";
import {BusquedaComponent} from "./busqueda/busqueda.component";

const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    canActivate: [LoginGuardGuard],
    children: [
      {path: 'dashboard', component: DashboardComponent, data: {title: 'Dashboard'}},
      {path: 'progress', component: ProgressComponent, data: {title: 'Progreso'}},
      {path: 'graficas1', component: Graficas1Component, data: {title: 'Grafica de Dona'}},
      {path: 'promesas', component: PromesasComponent, data: {title: 'Promesas'}},
      {path: 'rxjs', component: RxjsComponent, data: {title: 'Observables'}},
      {path: 'account-settings', component: AccountSettingsComponent, data: {title: 'Cambiar tema'}},
      {path: 'profile', component: ProfileComponent, data: {title: 'Perfil de usuario'}},
      {path: 'busqueda/:termino', component: BusquedaComponent, data: {title: 'Busqueda'}},
      //Mantenimientos
      {path: 'usuarios', component: UsuariosComponent, data: {title: 'Usuarios'}, canActivate: [AdminGuard]},
      {path: 'hospitales', component: HospitalesComponent, data: {title: 'Hospitales'}},
      {path: 'medicos', component: MedicosComponent, data: {title: 'Medicos'}},
      {path: 'medico/:id', component: MedicoComponent, data: {title: 'Actualizar medico'}},
      {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    ]
  }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
