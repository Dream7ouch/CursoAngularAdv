import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {URL_SERVICIOS} from "../../config/config";
import {Usuario} from "../../models/usuario.model";
import {Medico} from "../../models/medico.model";
import {Hospital} from "../../models/hospital.model";

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styles: []
})
export class BusquedaComponent implements OnInit {

  usuarios: Usuario[];
  medicos: Medico[];
  hospitales: Hospital[];

  constructor(public params: ActivatedRoute, public http: HttpClient) {
    this.params.params.subscribe(params => {
      let termino = params['termino'];
      this.buscar(termino);
    });
  }

  ngOnInit() {
  }

  buscar(termino: string){
    this.http.get(`${URL_SERVICIOS}/busqueda/todo/${termino}`).subscribe((res: any) => {
      this.usuarios = res.usuarios;
      this.medicos = res.medicos;
      this.hospitales = res.hospitales;
    });
  }

}
