import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsuarioService} from "../../services/usuario/usuario.service";
import {Usuario} from "../../models/usuario.model";
import {AutoUnsubscribe} from "../../decorators/index";
import {ModalUploadService} from "../../components/modal-upload/modal-upload.service";

declare var swal: any;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: []
})
@AutoUnsubscribe()
export class UsuariosComponent implements OnInit, OnDestroy {

  usuarios: Usuario[];
  desde: number = 0;
  totalRegistros: number = 0;
  cargando: boolean = true;

  constructor(private _usuarioService: UsuarioService, private _modalUploadService: ModalUploadService) { }

  ngOnInit() {
    this.cargando = true;
    this.cargarUsuarios();
    this._modalUploadService.notificaNuevaImagen.subscribe(() => this.cargarUsuarios());
  }

  ngOnDestroy(){
  }

  abrirModal(usuario: Usuario){
    this._modalUploadService.id = usuario._id;
    this._modalUploadService.tipo = 'usuarios';
  }

  cargarUsuarios(){
    this._usuarioService.cargarUsuarios(this.desde).subscribe((res: any) => {
      this.totalRegistros = res.total;
      this.usuarios = res.usuarios;
      this.cargando = false;
    });
  }

  cambiarDesde(valor: number){
    let desde = this.desde + valor;
    if (desde >= this.totalRegistros) return;
    if (desde > 0) this.desde += valor; else this.desde = 0;
    this.cargarUsuarios();
  }

  buscarUsuario(termino: string){
    if (termino.length <= 0){
      this.cargarUsuarios();
      return;
    }
    this.cargando = true;
    this._usuarioService.buscarUsuario(termino).subscribe(usuarios => {
      this.usuarios = usuarios;
      this.cargando = false;
    });
  }

  guardarUsuario(usuario: Usuario){
    this._usuarioService.actualizarUsuario(usuario).subscribe();
  }

  borrarUsuario(usuario: Usuario) {
    if (usuario._id === this._usuarioService.usuario._id) return;
    swal({
      title: '¿Esta Seguro?',
      text: `Esta a punto de eliminar a ${usuario.nombre}`,
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(borrar => {
      if (borrar) this._usuarioService.borrarUsuario(usuario._id).subscribe(() => this.cargarUsuarios());
    });
  }


}
