import {Component, OnDestroy, OnInit} from '@angular/core';
import {AutoUnsubscribe} from "../../decorators";

import {HospitalService} from "../../services/service.index";
import {ModalUploadService} from "../../components/modal-upload/modal-upload.service";

import {Hospital} from "../../models/hospital.model";

declare var swal: any;

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styleUrls: []
})
@AutoUnsubscribe()
export class HospitalesComponent implements OnInit, OnDestroy {

  hospitales: Hospital[];
  desde: number = 0;
  totalRegistros: number = 0;
  cargando: boolean = true;

  constructor(private _hospitalService: HospitalService, private _modalUploadService: ModalUploadService) { }

  ngOnInit() {
    this.cargando = true;
    this.cargarHospitales();
    this._modalUploadService.notificaNuevaImagen.subscribe(() => this.cargarHospitales());
  }

  ngOnDestroy(){}

  abrirModal(hospital: Hospital){
    this._modalUploadService.id = hospital._id;
    this._modalUploadService.tipo = 'hospitales';
  }

  crearHospital() {
    swal({
      title: 'Crear hospital',
      text: 'Ingrese el nombre del hospital',
      content: 'input',
      icon: 'info',
      buttons: true,
      dangerMode: true
    }).then( (nombre: string) => {
      if (!nombre || nombre.length == 0) return;
      this._hospitalService.crearHospital(nombre).subscribe(() => this.cargarHospitales());
    });
  }

  cargarHospitales(){
    this._hospitalService.cargarHospitales(this.desde).subscribe((res: any) =>{
      let {total, hospitales} = res;
      this.totalRegistros = total;
      this.hospitales = hospitales;
      this.cargando = false;
    });
  }

  buscarHospital(termino: string){
    if (termino.length <= 0){
      this.cargarHospitales();
      return;
    }
    this.cargando = true;
    this._hospitalService.buscarHospital(termino).subscribe(hospitales => {
      this.hospitales = hospitales;
      this.cargando = false;
    });
  }

  cambiarDesde(valor: number){
    let desde = this.desde + valor;
    if (desde >= this.totalRegistros) return;
    if (desde > 0) this.desde += valor; else this.desde = 0;
    this.cargarHospitales();
  }

  guardarHospital(hospital: Hospital){
    this._hospitalService.actualizarHospital(hospital).subscribe();
  }

  borrarHospital(hospital: Hospital) {
    swal({
      title: '¿Esta Seguro?',
      text: `Esta a punto de eliminar a ${hospital.nombre}`,
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(borrar => {
      if (borrar) this._hospitalService.borrarHospital(hospital._id).subscribe(() => this.cargarHospitales());
    });
  }

}
