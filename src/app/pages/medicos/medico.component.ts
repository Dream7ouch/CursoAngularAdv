import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {Hospital} from "../../models/hospital.model";
import {HospitalService, MedicoService} from "../../services/service.index";
import {Medico} from "../../models/medico.model";
import {ModalUploadService} from "../../components/modal-upload/modal-upload.service";

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: []
})
export class MedicoComponent implements OnInit {

  hospitales: Hospital[];
  medico: Medico = new Medico();
  hospital: Hospital = new Hospital('');

  constructor(public _hopitalService: HospitalService, public _medicoService: MedicoService,
              public router: Router, public params: ActivatedRoute, public _modalUploadService: ModalUploadService) {
  }

  ngOnInit() {
    this.cargarHospitales();
    this.params.params.subscribe(params => {
      let id = params['id'];
      if (id != 'nuevo') this.cargarMedico(id);
    });
    this._modalUploadService.notificaNuevaImagen.subscribe((res: any) => {
      this.medico.img = res.medico.img;
    });
  }

  cargarMedico(id: string) {
    this._medicoService.obtenerMedico(id).subscribe((medico: any) => {
      this.medico = medico;
      this.medico.hospital = medico.hospital._id;
      this.cambioHospital(this.medico.hospital);
    });
  }

  abrirModal(medico: Medico) {
    this._modalUploadService.id = medico._id;
    this._modalUploadService.tipo = 'medicos';
  }

  cargarHospitales() {
    this._hopitalService.cargarHospitales(0, 0).subscribe((res: any) => {
      this.hospitales = res.hospitales;
    });
  }

  cambioHospital(id: string) {
    if (!id) return;
    this._hopitalService.obtenerHospital(id).subscribe((res: any) => this.hospital = res.hospital);
  }

  guardarMedico(form: NgForm) {
    if (form.invalid) return;
    this._medicoService.guardarMedico(form.value).subscribe(medico => {
      this.medico._id = medico._id;
      this.router.navigate(['/medico', medico._id]);
    });
  }

}
