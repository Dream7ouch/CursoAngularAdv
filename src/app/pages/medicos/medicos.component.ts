import {Component, OnDestroy, OnInit} from '@angular/core';
import {AutoUnsubscribe} from "../../decorators";
import {Medico} from "../../models/medico.model";
import {MedicoService} from "../../services/service.index";

declare var swal: any;

@AutoUnsubscribe()
@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: []
})
export class MedicosComponent implements OnInit, OnDestroy {

  medicos: Medico[];
  desde: number = 0;
  totalRegistros: number = 0;
  cargando: boolean = true;

  constructor(private _medicoService: MedicoService) { }

  ngOnInit() {
    this.cargando = true;
    this.cargarMedicos();
  }

  ngOnDestroy() {}

  cargarMedicos(){
    this._medicoService.cargarMedicos().subscribe((res: any) => {
      let {medicos, total} = res;
      this.totalRegistros = total;
      this.medicos = medicos;
      this.cargando = false;
    });
  }

  borrarMedico(medico: Medico) {
    swal({
      title: '¿Esta Seguro?',
      text: `Esta a punto de eliminar a ${medico.nombre}`,
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(borrar => {
      if (borrar) this._medicoService.borrarMedico(medico._id).subscribe(() => this.cargarMedicos());
    });
  }

  buscarMedico(termino: string){
    if (termino.length <= 0){
      this.cargarMedicos();
      return;
    }
    this.cargando = true;
    this._medicoService.buscarMedico(termino).subscribe(medicos => {
      this.medicos = medicos;
      this.cargando = false;
    });
  }

  cambiarDesde(valor: number){
    let desde = this.desde + valor;
    if (desde >= this.totalRegistros) return;
    if (desde > 0) this.desde += valor; else this.desde = 0;
    this.cargarMedicos();
  }

}
