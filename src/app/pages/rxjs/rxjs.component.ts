import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Subscription} from "rxjs/Subscription";

import {map, filter, retry} from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit, OnDestroy {

  subscription: Subscription;


  constructor() {
    this.subscription = this.regresaObservable().subscribe(cont => console.log(cont), err => console.log(err), () => console.log('Terminado'));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  regresaObservable(): Observable<number> {
    return new Observable(observer => {
      let contador = 1;
      setInterval(() => {
        contador += 1;
        let salida = {
          valor: contador
        };
        observer.next(salida);
      }, 500);
    }).pipe(
      retry(2),
      map((res: any) => res && res.salida && res.salida.valor || 0),
      filter(valor => (valor % 2 == 1))
    );
  }

}
