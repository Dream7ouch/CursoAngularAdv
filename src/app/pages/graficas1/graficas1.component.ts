import { Component } from '@angular/core';

@Component({
  selector: 'app-graficas1',
  templateUrl: './graficas1.component.html'
})
export class Graficas1Component {

  graficos: any[] = [
    {
      labels: ['Con Frijoles', 'Con Natilla', 'Con Tocino'],
      data: [24, 30, 46],
      title: 'El pan se come con'
    },
    {
      labels: ['Hombres', 'Mujeres'],
      data: [4500, 6000],
      title: 'Entrevistados'
    },
    {
      labels: ['Si', 'No'],
      data: [95, 5],
      title: 'Sabe programar?'
    },
    {
      labels: ['Java', 'JavaScript', 'C#', 'Phyton'],
      data: [15, 80, 2, 3],
      title: 'Lenguajes utilizados'
    }
  ];

  constructor() { }
}
