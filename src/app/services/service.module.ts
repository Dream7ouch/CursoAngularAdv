import {NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {HttpClientModule} from "@angular/common/http";

import {
  UsuarioService,
  HospitalService,
  MedicoService,
  SettingsService,
  SidebarService,
  SharedService,
  LoginGuardGuard,
  AdminGuard,
  SubirArchivoService
} from "./service.index";

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    UsuarioService,
    HospitalService,
    MedicoService,
    SettingsService,
    SidebarService,
    SharedService,
    LoginGuardGuard,
    AdminGuard,
    SubirArchivoService
  ],
  declarations: []
})
export class ServiceModule {
}
