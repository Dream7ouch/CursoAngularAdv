import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {URL_SERVICIOS} from "../../config/config";
import {UsuarioService} from "../usuario/usuario.service";
import {Medico} from "../../models/medico.model";
import {map} from "rxjs/operators";

declare var swal: any;

@Injectable()
export class MedicoService {

  constructor(private http: HttpClient, public _usuarioService: UsuarioService) {
  }

  cargarMedicos() {
    return this.http.get(`${URL_SERVICIOS}/medico`);
  }

  obtenerMedico(id: string) {
    return this.http.get(`${URL_SERVICIOS}/medico/${id}`).pipe(
      map((res: any) => {
        return res.medico;
      })
    );
  }

  guardarMedico(medico: Medico) {
    if (medico._id) return this.actualizarMedico(medico);
    else return this.crearMedico(medico);
  }

  crearMedico(medico: Medico) {
    return this.http.post(`${URL_SERVICIOS}/medico?token=${this._usuarioService.token}`, medico)
      .pipe(map((res: any) => {
        swal('Medico creado', 'El medico se creo correctamente', 'success');
        return res.medico;
      }));
  }

  actualizarMedico(medico: Medico) {
    return this.http.put(`${URL_SERVICIOS}/medico/${medico._id}?token=${this._usuarioService.token}`, medico)
      .pipe(map((res: any) => {
        swal('Medico actualizado', `El medico ${medico.nombre} se actualizo correctamente`, 'success');
        return res.medico;
      }));
  }

  borrarMedico(id: string) {
    return this.http.delete(`${URL_SERVICIOS}/medico/${id}?token=${this._usuarioService.token}`)
      .pipe(map(() => swal('Medico borrado', 'El medico se elimino correctamente', 'success')));
  }

  buscarMedico(termino: string) {
    return this.http.get(`${URL_SERVICIOS}/busqueda/coleccion/medicos/${termino}`)
      .pipe(map((res: any) => res.medicos));
  }

}
