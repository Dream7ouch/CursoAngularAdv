import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

import {URL_SERVICIOS} from "../../config/config";
import {Usuario} from "../../models/usuario.model";
import {SubirArchivoService} from "../subir-archivo/subir-archivo.service";
import {map} from "rxjs/operators";

declare var swal: any;

@Injectable()
export class UsuarioService {

  usuario: Usuario;
  token: string;
  menu: any[];

  constructor(public http: HttpClient, public router: Router, public subirArchivo: SubirArchivoService) {
    this.cargarStorage();
  }

  estaLogueado() {
    return this.token ? this.token.length > 5 : false;
  }

  guardarStorage(id: string, token: string, usuario: Usuario, menu: any[]) {
    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));
    localStorage.setItem('menu', JSON.stringify(menu));
    this.usuario = usuario;
    this.token = token;
    this.menu = menu;
  }

  cargarStorage() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token') || '';
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
      this.menu = JSON.parse(localStorage.getItem('menu'));
    }
  }

  login(usuario: Usuario, recordar: boolean) {
    if (recordar) localStorage.setItem('email', usuario.email);
    else localStorage.removeItem('email');
    return this.http.post(URL_SERVICIOS + '/login', usuario).pipe(
      map((res: any) => {
        this.guardarStorage(res.id, res.token, res.usuario, res.menu);
        return true;
      })
    );
  }

  loginGoogle(token: string) {
    return this.http.post(URL_SERVICIOS + '/login/google', {token}).pipe(
      map((res: any) => {
        this.guardarStorage(res.id, res.token, res.usuario, res.menu);
        return true;
      })
    );
  }

  crearUsuario(usuario: Usuario) {
    return this.http.post(URL_SERVICIOS + '/usuario', usuario).pipe(
      map((resp: any) => {
        swal("Usuario creado", usuario.email, 'success');
      })
    );
  }

  actualizarUsuario(usuario: Usuario) {
    return this.http.put(URL_SERVICIOS + '/usuario/' + this.usuario._id + '?token=' + this.token, usuario).pipe(
      map((resp: any) => {
        if (usuario._id === this.usuario._id){
          let usuario: Usuario = resp.usuario;
          this.guardarStorage(usuario._id, this.token, usuario, this.menu);
        }
        swal('Usuario actualizado', usuario.nombre, 'success');
        return true;
      })
    );
  }

  cambiarImagen(archivo: File, id: string) {
    this.subirArchivo.subirArchivo(archivo, 'usuarios', id).then((res: any) => {
      this.usuario.img = res.usuario.img;
      swal('Imagen actualizada', this.usuario.nombre, 'success');
      this.guardarStorage(this.usuario._id, this.token, this.usuario, this.menu);
    }).catch(err => {
      swal('Error al actualizar imagen', this.usuario.nombre, 'error');
    });
  }

  cargarUsuarios(desde: number = 0){
    return this.http.get(URL_SERVICIOS + '/usuario?desde=' + desde);
  }

  buscarUsuario(termino: string){
    return this.http.get(URL_SERVICIOS + '/busqueda/coleccion/usuarios/' + termino).pipe(
      map((res: any) => res.usuarios)
    );
  }

  borrarUsuario(id: string){
    return this.http.delete(URL_SERVICIOS + '/usuario/' + id + '?token=' + this.token).pipe(
      map( () => swal('Usuario borrado', 'El usuario se elimino correctamente', 'success') )
    );
  }

  logout() {
    this.usuario = null;
    this.token = null;
    this.menu = [];
    localStorage.removeItem('id');
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('menu');
    this.router.navigate(['/login']);
  }

}
