import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {URL_SERVICIOS} from "../../config/config";

import {Hospital} from "../../models/hospital.model";
import {SubirArchivoService} from "../subir-archivo/subir-archivo.service";
import {UsuarioService} from "../usuario/usuario.service";
import {map} from "rxjs/operators";

declare var swal: any;

@Injectable()
export class HospitalService {

  constructor(public http: HttpClient, public subirArchivo: SubirArchivoService, public _usuarioService: UsuarioService) {
  }

  cargarHospitales(desde: number, limite?: number) {
    return this.http.get(`${URL_SERVICIOS}/hospital?desde=${desde}&limite=${limite}`);
  }

  obtenerHospital(id: string) {
    return this.http.get(`${URL_SERVICIOS}/hospital/${id}`);
  }

  crearHospital(nombre: string) {
    return this.http.post(`${URL_SERVICIOS}/hospital?token=${this._usuarioService.token}`, {nombre})
      .pipe(map((res: any) => res.hospital));
  }

  actualizarHospital(hospital: Hospital) {
    return this.http.put(`${URL_SERVICIOS}/hospital/${hospital._id}?token=${this._usuarioService.token}`, hospital)
      .pipe(map((res: any) => {
        swal('Hospital actualizado', `El hospital ${hospital.nombre} se actualizo correctamente`, 'success');
        return res.hospital;
      }));
  }

  borrarHospital(id: string) {
    return this.http.delete(`${URL_SERVICIOS}/hospital/${id}?token=${this._usuarioService.token}`)
      .pipe(map(() => swal('Hospital borrado', 'El hospital se elimino correctamente', 'success')));
  }

  buscarHospital(termino: string) {
    return this.http.get(`${URL_SERVICIOS}/busqueda/coleccion/hospitales/${termino}`)
      .pipe(map((res: any) => res.hospitales));
  }

}
