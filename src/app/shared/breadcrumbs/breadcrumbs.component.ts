import {Component, OnInit} from '@angular/core';
import {ActivationEnd, Router} from "@angular/router";
import {Title, Meta, MetaDefinition} from "@angular/platform-browser";

import {map, filter} from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html'
})
export class BreadcrumbsComponent implements OnInit {

  pagina: string;

  constructor(private router: Router, public _title: Title, public _meta: Meta) {
    this.getDataRoute().subscribe(data => {
      this.pagina = data.title;
      this._title.setTitle(data.title);
      let metaTag: MetaDefinition = {
        name: 'description',
        content: this.pagina
      };
      this._meta.updateTag(metaTag);
    });
  }

  ngOnInit() {
  }

  getDataRoute(){
    return this.router.events.pipe(
      filter(event => event instanceof ActivationEnd),
      filter((event: ActivationEnd) => event.snapshot.firstChild === null),
      map((event: ActivationEnd) => event.snapshot.data)
    );
  }

}
