import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-grafico-dona',
  template: `<h4 *ngIf="title">{{title}}</h4>
             <div style="display: block">
               <canvas baseChart
                        [data]="chartData"
                        [labels]="chartLabels"
                        [chartType]="chartType"></canvas>
             </div>`
})
export class GraficoDonaComponent {

  @Input() chartLabels:string[] = [];
  @Input() chartData:number[] = [];
  @Input() title:String = '';
  chartType:string = 'doughnut';

  constructor() { }

}
