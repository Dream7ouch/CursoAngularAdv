import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {SubirArchivoService} from "../../services/subir-archivo/subir-archivo.service";
import {ModalUploadService} from "./modal-upload.service";

declare var swal: any;

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {

  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  imagenSubir: File;
  imagenTemp: string;

  constructor(private _subirArchivoService: SubirArchivoService, public _modalUploadService: ModalUploadService) {
  }

  ngOnInit() {
  }

  seleccionImagen(archivo: File){
    this.imagenSubir = null;
    if(!archivo) return;
    if(archivo.type.indexOf('image') < 0){
      swal('Solo imagenes', 'El archivo seleccionado no es una imagen', 'error');
      return;
    }
    this.imagenSubir = archivo;
    let reader = new FileReader();
    reader.readAsDataURL(archivo);
    reader.onloadend = () => this.imagenTemp = reader.result;
  }

  cambiarImagen(){
    this._subirArchivoService.subirArchivo(this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id).then(res => {
      this.imagenSubir = null;
      this.imagenTemp = null;
      this._modalUploadService.id = null;
      this._modalUploadService.tipo = null;
      this._modalUploadService.notificaNuevaImagen.emit(res);
      this.closeAddExpenseModal.nativeElement.click();
    }).catch(err => {
      console.error(err);
    });
  }

}
