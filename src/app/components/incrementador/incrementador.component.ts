import {Component, ElementRef, EventEmitter, Input, Output, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html'
})
export class IncrementadorComponent {

  @ViewChild('inputNumber') inputNumber: ElementRef;

  @Input('title') leyenda: String = '';
  @Input() progreso: number = 0;

  @Output('progreso') cambioValor: EventEmitter<number> = new EventEmitter();

  constructor(private renderer: Renderer2) { }

  onChange(newValue: number){
    this.progreso = newValue;
    if (this.progreso >= 100) this.progreso = 100;
    if (this.progreso <= 0) this.progreso = 0;
    this.inputNumber.nativeElement.value = this.progreso;
    this.cambioValor.emit(this.progreso);
  }

  cambiarValor(valor){
    this.progreso += valor;
    if (this.progreso >= 100 ) this.progreso = 100;
    if (this.progreso <= 0) this.progreso = 0;
    this.inputNumber.nativeElement.focus();
    this.cambioValor.emit(this.progreso);
  }

}
