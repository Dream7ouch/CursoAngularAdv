import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {ChartsModule} from "ng2-charts";

import {IncrementadorComponent} from "./incrementador/incrementador.component";
import { GraficoDonaComponent } from './grafico-dona/grafico-dona.component';
import { ModalUploadComponent } from './modal-upload/modal-upload.component';
import {PipesModule} from "../pipes/pipes.module";
import {ModalUploadService} from "./modal-upload/modal-upload.service";

@NgModule({
  declarations: [
    IncrementadorComponent,
    GraficoDonaComponent,
    ModalUploadComponent
  ],
  exports: [
    IncrementadorComponent,
    GraficoDonaComponent,
    ModalUploadComponent
  ],
  imports: [
    FormsModule,
    ChartsModule,
    CommonModule,
    PipesModule
  ],
  providers: [
    ModalUploadService
  ]
})

export class ComponentsModule {
}
