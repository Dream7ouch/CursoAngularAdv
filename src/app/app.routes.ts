import {RouterModule, Routes} from "@angular/router";

import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./login/register.component";
import {NopagefoundComponent} from "./shared/nopagefound/nopagefound.component";

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent, data: {title: 'Iniciar Sesion'}},
  {path: 'register', component: RegisterComponent, data: {title: 'Registrarse'}},
  {path: '**', component: NopagefoundComponent, data: {title: 'No encontrado'}},
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, {useHash: true});
