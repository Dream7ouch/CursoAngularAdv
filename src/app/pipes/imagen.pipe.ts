import { Pipe, PipeTransform } from '@angular/core';
import {URL_SERVICIOS} from "../config/config";

@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(img: string, tipo: string = 'usuario'): string {
    let url = URL_SERVICIOS + '/img';
    let urlImagen;
    if (!img) return url + '/imagen/not-found';
    if (img.indexOf('http') > 0) return img;
    if (img.indexOf('base64') > 0) return img;
    else {
      switch (tipo) {
        case 'usuario':
          urlImagen = url + '/usuarios/' + img;
          break;
        case 'medico':
          urlImagen = url + '/medicos/' + img;
          break;
        case 'hospital':
          urlImagen = url + '/hospitales/' + img;
          break;
        default:
          urlImagen = url + '/usuarios/xxx';
          break;
      }
    }
    return urlImagen;
  }

}
