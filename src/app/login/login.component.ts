import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router"
import {NgForm} from "@angular/forms";

import {UsuarioService} from "../services/usuario/usuario.service";

import {Usuario} from "../models/usuario.model";

declare var swal: any;

declare function initPlugins();

declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  recuerdame: boolean = false;

  auth2: any;

  constructor(public _usuarioService: UsuarioService, public router: Router) {
  }

  ngOnInit() {
    initPlugins();
    this.googleInit();
    this.email = localStorage.getItem('email') || '';
    if (this.email.length > 1) this.recuerdame = true;
  }

  ingresar(forma: NgForm) {
    if (forma.invalid) return;
    let usuario = new Usuario(null, forma.value.email, forma.value.password);
    this._usuarioService.login(usuario, forma.value.recuerdame).subscribe(resp => {
      this.router.navigate(['/dashboard']);
    });
  }

  googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '49814356173-f68b12sr36h9al7rrfn7spg98h5godau.apps.googleusercontent.com ',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
      this.attachSignIn(document.getElementById('btnGoogle'));
    });
  }

  attachSignIn(element) {
    this.auth2.attachClickHandler(element, {}, googleUser => {
      // let profile = googleUser.getBasicProfile();
      let token = googleUser.getAuthResponse().id_token;
      this._usuarioService.loginGoogle(token).subscribe(() => this.router.navigate(['/dashboard']));
    });
  }

}
