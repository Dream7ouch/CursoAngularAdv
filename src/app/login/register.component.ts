import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

import {UsuarioService} from "../services/service.index";
import {Usuario} from "../models/usuario.model";

declare var swal: any;
declare function initPlugins();

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./login.component.scss']
})
export class RegisterComponent implements OnInit {

  forma: FormGroup;

  constructor(public _usuarioService: UsuarioService, public router: Router) { }

  ngOnInit() {
    initPlugins();
    this.forma = new FormGroup({
      nombre: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      password2: new FormControl(null, Validators.required),
      condiciones: new FormControl(false)
    }, {validators: this.comparar('password', 'password2')});
  }

  comparar(campo1: string, campo2: string){
    return (group: FormGroup) => {

      let pass1 = group.controls[campo1].value;
      let pass2 = group.controls[campo2].value;

      if (pass1 == pass2) return null;

      return {
        comparar: true
      };
    }
  }

  registrarUsuario(){
    if (this.forma.invalid) return;
    if (!this.forma.value.condiciones) {
      swal("Importante", "Debes aceptar los terninos", "warning");
      return;
    }
    let usuario = new Usuario(this.forma.value.nombre, this.forma.value.email, this.forma.value.password);

    this._usuarioService.crearUsuario(usuario).subscribe(res=> {
      this.router.navigate(['/login']);
    }, error1 => {
      swal("Usuario duplicado", usuario.email, 'error');
    });
  }

}
